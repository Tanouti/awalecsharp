﻿using ProjectCsharp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.ComponentModel;
using ProjectCsharp.Db;

namespace ProjectCsharp
{
    /// <summary>
    /// Logique d'interaction pour LocalGame.xaml
    /// </summary>
    public partial class LocalGame : Window
    {
        Plateau plateau = Plateau.Instance;
        List<Button> btnJ1 = new List<Button>();
        List<Button> btnJ2 = new List<Button>();
        DatabaseContext context = new DatabaseContext();
        //SQLiteConnection db = new SQLiteConnection(@"Data Source = c:\mydb.db, Version=3");

        public LocalGame()
        {
            InitializeComponent();
            InitListsBtns();
            plateau.changementDeJoueur += Plateau_changementDeJoueur;

            // db.Open();
        }
     
        private void Plateau_changementDeJoueur(string tour)
        {
            UpgradePlateau(tour);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(0);
            }  
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(1);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(2);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(3);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(4);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(5);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(6);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(7);
            }
            //else
            //{
            //    SaveGame();
            //}
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(8);
            }
            //else
            //{
            //    SaveGame();
            //}

        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(9);
            }
         
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(10);
            } 
            else
            {
                SaveGame();
            }
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            if (!AnnounceWinner())
            {
                plateau.JouerCoup(11);
            }   
            else
            {
                SaveGame();
            }
        }

        public void InitListsBtns()
        {
            //liste de joueur1
            btnJ1.Add(btn0); btnJ1.Add(btn1);
            btnJ1.Add(btn2); btnJ1.Add(btn3);
            btnJ1.Add(btn4); btnJ1.Add(btn5);

            //liste de Joueur2
            btnJ2.Add(btn6); btnJ2.Add(btn7);
            btnJ2.Add(btn8); btnJ2.Add(btn9);
            btnJ2.Add(btn10); btnJ2.Add(btn11);

            //Joueur1 qui commence;
            foreach (Button bt in btnJ2)
            {
                bt.Background = Brushes.SaddleBrown;
                bt.IsEnabled = false;
            }
        }

        public void UpgradePlateau(string indice)
        {
            if (indice == "J1")
            {
                foreach (Button bt in btnJ1)
                {
                    bt.Background = Brushes.SaddleBrown;
                    bt.IsEnabled = true;                 
                }
                foreach (Button bt in btnJ2)
                {
                    bt.Background = Brushes.SaddleBrown;
                    bt.IsEnabled = false;                  
                }
            }
            else
            {
                foreach (Button bt in btnJ1)
                {
                    bt.Background = Brushes.SaddleBrown;
                    bt.IsEnabled = false;
                    
                }
                foreach (Button bt in btnJ2)
                {
                    bt.Background = Brushes.SaddleBrown;
                    bt.IsEnabled = true;
                  
                }
            }
   
        }

        public Boolean AnnounceWinner()
        {
            Joueur joueur1 = plateau.J1;
            Joueur joueur2 = plateau.J2;
            if (plateau.J1.Score > 24)
            {

                Console.WriteLine("Joueur1 a gagné");
                SaveGame();
                MessageBoxResult result = MessageBox.Show("Winnner is "+ joueur1.Nom +" Do you want to Restart the Game?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    plateau.J1.Score = 0;
                    plateau.J2.Score = 0;
                    plateau.InitialiserJeu();
                    //Application.Current.Shutdown();
                }
                else
                {
                    this.Close();
                }
                return true;
            }
            else if (plateau.J2.Score > 24)
            {
             
                Console.WriteLine("Joueur2 a gagné");
                MessageBoxResult result = MessageBox.Show("Winnner is " + joueur2.Nom + " Do you want to Restart the game?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                SaveGame();
                if (result == MessageBoxResult.Yes)
                {
                    plateau.J1.Score = 0;
                    plateau.J2.Score = 0;
                    plateau.InitialiserJeu();
                    //Application.Current.Shutdown();
                }
                else
                {
                    this.Close();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SaveGame()
        {
            Historique historique = new Historique()
            {
                Joueur1 = plateau.J1.Nom,
                Joueur2 = plateau.J2.Nom,
                Score = plateau.J1.Score + "/" + plateau.J2.Score
            };
            context.Historique.Add(historique);
            context.SaveChangesAsync();
        }

    }
}
