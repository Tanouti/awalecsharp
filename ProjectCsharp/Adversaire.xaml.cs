﻿using ProjectCsharp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectCsharp
{
    /// <summary>
    /// Logique d'interaction pour Adversaire.xaml
    /// </summary>
    public partial class Adversaire : Window
    {
        public Adversaire()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Plateau.Instance.J1.Nom = Plateau.Instance.NomJoueur1;
            Plateau.Instance.J2.Nom = Plateau.Instance.NomJoueur2;
            Plateau.Instance.InitialiserJeu();
            LocalGame lg = new LocalGame();
            lg.ShowDialog();
        }
    }
}
