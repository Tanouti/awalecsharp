﻿using ProjectCsharp.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectCsharp
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        Serveur s = new Serveur();
        NetworkGame nt;


        public MainWindow()
        {
          
        }

        private void btnclick(object sender, RoutedEventArgs e)
        {
            //LocalGame lg = new LocalGame();
            // lg.ShowDialog();
            Adversaire ad = new Adversaire();
             ad.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {                 
            s.Start(s.GetIp());
            nt = new NetworkGame(s);
            nt.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NetworkGame nt = new NetworkGame(s);
            nt.ShowDialog();
           
                
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            HistoriqueView hist = new HistoriqueView();
            hist.ShowDialog();
        }
    }
}
