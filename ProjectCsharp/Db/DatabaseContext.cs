﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCsharp.Db
{
        public class DatabaseContext : DbContext
        {
            public DatabaseContext() :
                base(new SQLiteConnection()
                {
                    ConnectionString = new SQLiteConnectionStringBuilder() { DataSource = "C:\\Users\\mtanouti\\Desktop\\projetC#.db", ForeignKeys = true }.ConnectionString
                }, true)
            {
            }
            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
                base.OnModelCreating(modelBuilder);
            }

            public DbSet<Historique> Historique { get; set; }
        }
    }

