﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Linq.Mapping;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCsharp.Db
{
    [Table(Name = "Historique")]
    public class Historique
    {
        [Column(Name = "ID", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int ID { get; set; }

        [Column(Name = "Joueur1", DbType = "VARCHAR")]
        public string Joueur1 { get; set; }

        [Column(Name = "Joueur2", DbType = "VARCHAR")]
        public string Joueur2 { get; set; }

        [Column(Name = "Score", DbType = "VARCHAR")]
        public string Score { get; set; }
        
    }
}
