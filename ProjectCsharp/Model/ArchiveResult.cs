﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCsharp.Model
{
    public class ArchiveResult
    {
        string id;
        string joueur1;
        string joueur2;
        string score;

        public ArchiveResult()
        {

        }

        public string Id { get => id; set => id = value; }
        public string Joueur1 { get => joueur1; set => joueur1 = value; }
        public string Joueur2 { get => joueur2; set => joueur2 = value; }
        public string Score { get => score; set => score = value; }
    }
}
