﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCsharp.Model
{
    public class Joueur : INotifyPropertyChanged
    {
        private string nom;
        private int score = 0;
        private string indexJoueur;
        public event PropertyChangedEventHandler PropertyChanged;
        public Joueur(string nom, string indexJoueur)
        {
            this.nom = nom;
            this.indexJoueur = indexJoueur;
        }
        public Joueur()
        {

        }
        public string Nom { get => nom; set => nom = value; }
        public string IndexJoueur { get => indexJoueur; set =>  indexJoueur = value;    }
        public int Score { get => score; set { score = value; PropertyChanged(this, new PropertyChangedEventArgs("Score")); } }
        public void AddScore(int score)
        {
            Score+= score;
        }
        public String toString()
        {
            return "Joueur [nom : " + nom + " , score = " + score + " , side : "
                    + indexJoueur + " ]";
        }
    }
}
