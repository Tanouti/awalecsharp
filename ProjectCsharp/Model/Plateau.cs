﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace ProjectCsharp.Model
{
    public class Plateau
    {
        private static Plateau instance = null;
        private static readonly Plateau padlock = new Plateau();
        static int nbrTroux = 12;
        int nbrGraine = 48;
        public ObservableCollection<int> Trous { set; get; }
        public string NomJoueur1 { set; get; }
        public string NomJoueur2 { set; get; }
        private Joueur j1 = new Joueur("", "J1");
        private Joueur j2 = new Joueur("", "J2");
        private string tour = "J1";
        private string bloc;

        //singleton
        public static Plateau Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Plateau();
                    }
                    return instance;
                }
            }
        }
        //LocalGame lc = new LocalGame();
        private Plateau()
        {
            Trous = new ObservableCollection<int>();
            Init();
        }
        public Joueur J1 { get => j1; set => j1 = value; }
        public Joueur J2 { get => j2; set => j2 = value; }
        public string Bloc { get => bloc; set => bloc = value; }
        public string Tour { get => tour; set => tour = value; }

        public int GetTrou(int id)
        {
            return Trous[id];
        }

        public void SetTrou(int id, int value)
        {
            Trous[id] = value;
        }

        public void AjoutGraine(int index)
        {
            Trous[index]++;
        }

        public int GetNbrGraine()
        {
            int resultat = 0;
            foreach (int gr in Trous)
            {
                resultat += gr;
            }
            return resultat;
        }

        public void Init()
        {
            for (int i = 0; i < nbrTroux; i++)
            {
                Trous.Add(4);
            }

        }
        public void InitialiserJeu()
        {
            for (int i = 0; i < nbrTroux; i++)
            {
                Trous[i]=4;
            }
        }

        public int JouerCoup(int id)
        {
            int benefice = 0;
            int statusJeu = 1;
            int nbrGraineCellule = GetTrou(id);
            if (nbrGraineCellule == 0 && IllegalCoup(id))
            {
                statusJeu = -1;
            }
            else
            {
                SetTrou(id, 0);
                while (nbrGraineCellule > 0)
                {
                    
                    if (id == 11)
                    {
                        id = 0;
                        AjoutGraine(id);
                        nbrGraineCellule--;
                        if (nbrGraineCellule == 0)
                        {
                            benefice = recolteGraine(id);
                        }
                    }
                    else
                    {                        
                        AjoutGraine(id + 1);                       
                        id++;
                        nbrGraineCellule--;
                        if (nbrGraineCellule == 0)
                        {
                            benefice = recolteGraine(id);
                        }
                    }
                }
            

            if (tour == "J1")
            { 
                tour = "J2";}
            else { 
                tour = "J1";}
            changementDeJoueur?.Invoke(tour);}
            return benefice;


        }

        public int recolteGraine(int indexOfTrouFinal)
        {
            int gains = 0;
            if (tour == "J1")
            {
                if (indexOfTrouFinal > 5)
                {
                    for (int i = indexOfTrouFinal; i > 5; i--)
                    {
                        if (GetTrou(i) == 2 || GetTrou(i) == 3)
                        {
                            gains = GetTrou(i);
                            J1.AddScore(gains);
                            SetTrou(i, 0);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

            }
            else
            {
                if (indexOfTrouFinal < 6)
                {
                    for (int i = indexOfTrouFinal; i >= 0; i--)
                    {
                        if (GetTrou(i) == 2 || GetTrou(i) == 3)
                        {
                            gains += GetTrou(i);
                            J2.AddScore(gains);
                            SetTrou(i, 0);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            return gains;
        }

        public delegate void changeJoueur(string tour);
        public event changeJoueur changementDeJoueur;

        public Boolean IllegalCoup(int id)
        {
            int nbrGraine = GetTrou(id);
            int somme = NbrGraineAdversaire(id);
            if (id < 6)
            {             
                if (somme == 0 && id+nbrGraine<6)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (somme == 0 && id + nbrGraine < 12)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           
        }
        public int NbrGraineAdversaire(int id)
        {
            int somme=0;
            if (id < 6)
            {
                for (int i = 6; i < 12; i++)
                {
                    somme += GetTrou(i);
                }
                return somme;
            }
            else
            {
                for (int i = 0; i<6; i++)
                {
                    somme += GetTrou(i);
                }
                return somme;
            }
           

        }
       
    }
}
