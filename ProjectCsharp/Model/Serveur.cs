﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectCsharp.Model
{
    public class Serveur
    {
        static Socket soc;
        static Socket acc;
        static int port = 9000;
        static IPAddress ip;
        static Thread rec;
        static string name;
        Plateau p = Plateau.Instance;

        public Plateau P { get => p; set => p = value; }

        public string GetIp()
        {
            string hostname = Dns.GetHostName();
            IPHostEntry ipentry = Dns.GetHostEntry(hostname);
            IPAddress[] addr = ipentry.AddressList;
            return addr[addr.Length - 1].ToString();
        }


        public void RecV()
        {
            while (true)
            {
                Thread.Sleep(500);
                byte[] buffer = new byte[300];
                int rece = acc.Receive(buffer, 0, buffer.Length, 0);
                Array.Resize(ref buffer, rece);
                Console.WriteLine(Encoding.Default.GetString(buffer));
                string coup = Encoding.Default.GetString(buffer);
                p.JouerCoup(Convert.ToInt32(coup));
            }
        }



        public void Start(string nameJoueur)
        {
            rec = new Thread(RecV);
            Console.WriteLine("Your Local Ip is " + GetIp());
            Console.WriteLine("Please enter your name");
            name = nameJoueur;
            Console.WriteLine("Please enter HostPort");
            string prt ="8080";

            try
            {
                port = Convert.ToInt32(prt);
            }
            catch
            {
                port = 9000;
            }

            ip = IPAddress.Parse(GetIp());

            IPAddress ip1 = ip.MapToIPv4();
            soc = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
            if (!Socket.OSSupportsIPv6)
            {
                Console.WriteLine("Le système ne supporte pas les adresses IPv6.");
            }
            soc.Bind(new IPEndPoint(ip, port));
            soc.Listen(0);            
            acc = soc.Accept();
            rec.Start();
            //send("issam");


        }
        public void send(string message)
        {
            
                byte[] sdata = Encoding.Default.GetBytes(message);
                acc.Send(sdata, 0, sdata.Length, 0);
                p.JouerCoup(Convert.ToInt32(message));
         
        }
    }



}

