﻿using ProjectCsharp.Db;
using ProjectCsharp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectCsharp
{
    /// <summary>
    /// Logique d'interaction pour HistoriqueView.xaml
    /// </summary>
    public partial class HistoriqueView : Window
    {
        DatabaseContext context = new DatabaseContext();
        List<ArchiveResult> hist = new List<ArchiveResult>();
        public HistoriqueView()
        {
            Console.WriteLine("ok");
            var data = context.Historique.ToList();
            foreach (var item in data)
            {
                Hist.Add(new ArchiveResult()
                {
                    Id = Convert.ToString(item.ID),
                    Joueur1 = item.Joueur1,
                    Joueur2 = item.Joueur2,
                    Score = item.Score
                });
                Console.WriteLine("ID : {0}  Joueur1 : {1}  Joueur2 : {2}   Score : {3}", item.ID, item.Joueur1, item.Joueur2, item.Score);
            }
            InitializeComponent();
            this.DataContext = Hist;
        }

        public List<ArchiveResult> Hist { get => hist; set => hist = value; }
    }
}
