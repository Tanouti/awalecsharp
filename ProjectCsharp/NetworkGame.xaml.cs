﻿using ProjectCsharp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectCsharp
{
    /// <summary>
    /// Logique d'interaction pour NetworkGame.xaml
    /// </summary>
    public partial class NetworkGame : Window
    {
        Plateau plateau = Plateau.Instance;
        Client c = new Client();
        static int indice = 0;
        Serveur s ;

        public Client C { get => c; set => c = value; }
        public Serveur S { get => s; set => s = value; }

        public NetworkGame(Serveur s)
        {   
            InitializeComponent();
            indice++;
            this.s = s;
            C.Start(C.GetIp());
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(0));
            this.s.P.JouerCoup(0);
            plateau.JouerCoup(0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(1));
            plateau.JouerCoup(1);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(2));
            plateau.JouerCoup(2);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(3));
            plateau.JouerCoup(3);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(4));
            plateau.JouerCoup(4);
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(5));
            plateau.JouerCoup(5);
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(6));
            plateau.JouerCoup(6);
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(7));
            plateau.JouerCoup(7);
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(8));
            plateau.JouerCoup(8);
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(9));
            plateau.JouerCoup(9);
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(10));
            plateau.JouerCoup(10);
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            C.Send(Convert.ToString(11));
            plateau.JouerCoup(11);
        }
    }
}
